class ArticleDecorator < Draper::Decorator
  delegate_all

  def html_decode(text)
    HTMLEntities.new.decode(text)
  end

  def publication_status
    # if published?
    #   "Published at #{published_at}"
    # else
    #   "Unpublished"
    # end

    published? ? "<strong>Published at #{published_at}</strong>".html_safe : "<mark>Unpublished</mark>".html_safe

  end

  def published_at
    object.published_at.strftime("%A, %B %e")
  end

  def bolded_content
    h.content_tag(:strong, object.content)
  end

  def emphatic
    h.content_tag(:strong, "Awesome")
  end

  def my_class
    published? ? "published" : ""
  end

  def green
    published? ? "green" : ""
  end

  def with_style
    published? ? "font-size: 25px;" : ""
  end
end
